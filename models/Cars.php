<?php

class Cars 
{
    function all_cars()
    {
        $sql = DB::getConnection()->query('select * from `cars`');
        $cars = $sql->fetchAll(PDO::FETCH_ASSOC);

        return $cars;
    }

    function get_car()
    {
      
        $sql = DB::getConnection()->query('SELECT * FROM `cars`;');
        $cars = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $cars;
    }

    function save_car($id, $car_name, $car_brand, $car_description, $car_price, $car_image)
    {

        $sql = DB::getConnection()->query('UPDATE `cars` SET `car_name` = "' . $car_name . '" , `car_brand` = "' . $car_brand . '",
        `car_description` = "' . $car_description . '", `car_price` = "' . $car_price . '", `car_image` = "' . $car_image . '"
        WHERE `car_id` = "' . $id . '"');

    }

    function del_car($id)
    {

        $sql = DB::getConnection()->query('DELETE FROM `cars` WHERE `car_id` = "' . $id . '" ;');
    }

    function add_car($car_name, $car_brand, $car_description, $car_price, $car_image, $car_categor_id)
    {

        $sql = DB::getConnection()->query('INSERT INTO `cars` (`car_name`, `car_image`, `car_brand`, `car_description`, `car_price`, `car_categor_id`) VALUE ("' . $car_name . '", "' . $car_image . '","' . $car_brand . '","' . $car_description . '","' . $car_price . '", "' . $car_categor_id . '")');

    }

    function get_category_cars($category_id) {
        $sql = DB::getConnection()->query('select * from `cars` where `car_categor_id` = "'. $category_id .'"');
        $cars_category = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $cars_category;

    }


}
