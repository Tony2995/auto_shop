<?php

class Order {

    function all_orders() {
      
        $sql = DB::getConnection()->query('SELECT * FROM `order` WHERE `order_status` = 1;');
        $orders = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $orders;
    }

    function new_order($order_car, $order_name, $order_city, $order_email, $order_phone)
    {
        
        $sql = DB::getConnection()->query('INSERT INTO `order` (`order_name`, `order_city`, `order_email`, `order_phone`, `order_car`, `order_status`) VALUE ("' . $order_name . '", "' . $order_city . '","' . $order_email . '","' . $order_phone . '","' . $order_car. '", "'. 1 .'")');

    }

    function order_access($id) {

        $sql = DB::getConnection()->query('UPDATE `order` SET `order_status` = "'. 0 .'" WHERE `order_id` = "'. $id .'"');

    }

    function get_info($car_id) {

        $sql = DB::getConnection()->query('select `order`.*, `cars`.* from `cars` inner join `order` on `order`.`order_car` = `cars`.`car_id` and `order_id` = "'. $car_id .'"');
        $order = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $order;
    }

    function get_orders_id($id) {

        $sql = DB::getConnection()->query('SELECT * FROM `order` WHERE `order_id` = "'.$id.'";');
        $orders = $sql->fetchAll(PDO::FETCH_ASSOC);
        return $orders;
    }

}