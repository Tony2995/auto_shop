
<? include 'view/header.php'; ?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Город</th>
                                <th>Email</th>
                                <th>Телефон</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>

                            <? foreach ($orders as $order) { ?>
                                <tr>
                                    <td><? echo $order['order_name'] ?></td>
                                    <td><? echo $order['order_city'] ?></td>
                                    <td><? echo $order['order_email'] ?></td>
                                    <td><? echo $order['order_phone'] ?></td>
                                    <td>
                                        <form action=" " method="post">
                                            <input name="order_id" type="hidden" value="<? print $order['order_id'] ?>">

                                            <input type="submit" id="submit" class="btn btn-danger" value="Продать">
                                        </form>

                                    </td>
                                    <td>
                                        <a href="/admin/order?id=<? echo $order['order_id'] ?>" class="btn btn-info">Информация</a>
                                    </td>
                                </tr>
                            <? } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

        </div>
    </div>

</div>

<? include 'view/footer.php'; ?>
