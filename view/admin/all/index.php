<? include 'view/header.php'; ?>
<div class="container-fluid" style="height: 100%; padding-top: 50px;">
    <div class="row" style="height: 100%">
        <div class="col-md-2 left-menu">
            <div class="list-group">
                <? foreach ($categories as $categor) { ?>
                    <a href="<? echo $categor['categor_id']; ?>"
                       class="list-group-item"><? echo $categor['categor_name']; ?></a>
                <? } ?>
            </div>
        </div>
        <div class="col-md-10" style="padding-top: 22px;">
            <div class="container">
                <div class="row">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Наименование</th>
                            <th>Марка</th>
                            <th>Описание</th>
                            <th>Стоимость</th>
                            <th>Изображение</th>
                            <th>Подробнее</th>
                            <th>&nbsp</th>
                        </tr>
                        </thead>
                        <tbody>

                        <? foreach ($cars as $car) { ?>
                            <tr>
                                <td><? echo $car['car_name'] ?></td>
                                <td><? echo $car['car_brand'] ?></td>
                                <td><? echo $car['car_description'] ?></td>
                                <td><? echo 'Price: ' . $car['car_price'] . ' RUB' ?></td>
                                <td><? echo $car['car_image'] ?></td>
                                <td><p><a href="<? echo '/admin/change?id=' . $car['car_id'] ?>" class="btn btn-info"
                                          role="button">Изменить</a></p></td>
                                <td>
                                    <form action="" method="post">
                                        <input name="id" type="hidden" value="<? print $car['car_id'] ?>">

                                        <input type="submit" id="submit" class="btn btn-danger" value="Удалить">
                                    </form>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<? include 'view/footer.php'; ?>
