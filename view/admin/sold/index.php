
<? include 'view/header.php'; ?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Кто продал</th>
                            <th>Покупатель</th>
                            <th>Проданное авто</th>
                            <th>Телефон покупателя</th>
                            <th>Город</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <? foreach ( $sold_orders as $sold) { ?>
                            <tr>
                                    <td><? echo $sold['emp_name'] ?></td>
                                    <td><? echo $sold['order_name'] ?></td>
                                    <td><? echo $sold['car_name'] ?></td>
                                    <td><? echo $sold['order_phone'] ?></td>
                                    <td><? echo $sold['order_city'] ?></td>

                                <td>
                                    <a href="/admin/order?id=<? echo $sold['sale_id']; ?>" class="btn btn-info">Информация</a>
                                </td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>

<? include 'view/footer.php'; ?>
