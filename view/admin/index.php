<? include 'view/header.php'; ?>
<!--

добавить авто
удалить авто
изменить информацию об авто

изменить категории
добавить категории удлаить категории
посмотреть количетсво авто каждой категории

оформленные автомобили
проданные авто

наличие на складе

-->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Авто</h3>
            <div class="row">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="admin/all" class="btn btn-default">Посмотреть все</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="admin/add" class="btn btn-default">Добавить</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="admin/sold" class="btn btn-default">Проданные</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="admin/orders" class="btn btn-default">Заявки</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="admin/categories" class="btn btn-default">категории</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<? include 'view/footer.php'; ?>
