<? include 'view/header.php'; ?>
<div class="container-fluid" style="height: 100%; padding-top: 100px;">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" action="" method="POST" >
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Наименование</label>
                        <div class="col-md-4">
                            <input id="name" name="car_name" type="text" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="city">Марка</label>
                        <div class="col-md-4">
                            <input id="city" name="car_brand" type="text" class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Описание</label>
                        <div class="col-md-4">
                            <input id="email" name="car_description" type="text" class="form-control input-md">

                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Стоимость</label>
                        <div class="col-md-4">
                            <input id="email" name="car_price" type="text" class="form-control input-md">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Изменить изображение</label>
                        <div class="col-md-4">
                            <input type="file" name="car_image" id="fileToUpload" style="padding-top: 6px;">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Класс автомобиля</label>
                        <div class="col-md-4">
                            <select class="selectpicker" name="car_categor_id">

                                <? foreach ($categories as $categor) { ?>
                                    <option value="<? echo $categor['categor_id'] ?>"><? echo $categor['categor_name'] ?></option>
                                <? } ?>

                            </select>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email"></label>
                        <div class="col-md-4">
                            <input type="submit" class="btn btn-info" value="Сохранить изменения">
                        </div>
                    </div>

                </fieldset>

            </form>
        </div>

    </div>


</div>

<? include 'view/footer.php'; ?>
