
<? include 'view/header.php'; ?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Город</th>
                            <th>Email</th>
                            <th>Телефон</th>


                        </tr>
                        </thead>
                        <tbody>

                        <? foreach ($orders as $order) { ?>
                            <? if ($_GET['id'] === $order['order_id']) { ?>
                                <tr>
                                    <td><? echo $order['order_name'] ?></td>
                                    <td><? echo $order['order_city'] ?></td>
                                    <td><? echo $order['order_email'] ?></td>
                                    <td><? echo $order['order_phone'] ?></td>
                                </tr>
                            <? } ?>
                        <? } ?>


                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <h1>Инофрмация о заказе</h1>
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Наименование</th>
                            <th>Марка</th>
                            <th>Описание</th>
                            <th>Стоимость</th>
                            <th>Изображение</th>
                        </tr>
                        </thead>
                        <tbody>

                        <? foreach ($orders as $order) { ?>
                            <tr>
                                <td><? echo $order['car_name'] ?></td>
                                <td><? echo $order['car_brand'] ?></td>
                                <td><? echo $order['car_description'] ?></td>
                                <td><? echo 'Price: ' . $order['car_price'] . ' RUB' ?></td>
                                <td><img src="<? echo MediaLink('view/images/' . $order['car_image']) ?>" alt="" class="img-responsive"></td>

                            </tr>
                        <? } ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
</div>


<? include 'view/footer.php'; ?>
