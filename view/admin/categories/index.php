<? include 'view/header.php'; ?>

<div class="container-fluid" style="height: 100%; padding-top: 100px;">
    <div class="row">
        <div class="col-md-12">

            <form class="form-horizontal" action="" method="POST">
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Наименование категории</label>
                        <div class="col-md-4">
                            <input name="categor_name" type="text" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name"></label>
                        <div class="col-md-4">
                            <input type="submit" class="btn btn-info" value="Добавить" >
                        </div>
                    </div>
                </fieldset>
            </form>


            <form class="form-horizontal" action="" method="post">
                <fieldset>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">Удалить категорию</label>
                        <div class="col-md-4">
                            <select class="selectpicker" name="categor_id">

                                <? foreach ($categories as $categor) { ?>
                                    <option value="<? echo $categor['categor_id'] ?>"><? echo $categor['categor_name'] ?></option>
                                <? } ?>

                            </select>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name"></label>
                        <div class="col-md-4">
                            <input type="submit" id="del_btn" class="btn btn-info" value="Удалить" >
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

    </div>


</div>


<? include 'view/footer.php'; ?>
