<? include 'view/header.php'; ?>


<div class="container-fluid" style="height: 100%; padding-top: 50px;">
    <div class="row" style="height: 100%">
        <div class="col-md-2 left-menu">
            <div class="list-group">
                <? foreach ($categories as $categor) { ?>
                      <a href="<? echo $categor['categor_id']; ?>" class="list-group-item"><? echo $categor['categor_name']; ?></a>
                <? } ?>
            </div>
        </div>
        <div class="col-md-10" style="padding-top: 22px;">
            <div class="container">
              <div class="row">

                  <div class="col-md-8">



                        <? foreach ($cars as $car) { ?>

                            <? if ($_GET['id'] === $car['car_id']) { ?>

                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <div class="row">
                                  <div class="col-md-3">
                                      <h3 class="panel-title"><? echo $car['car_name'] ?></h3>
                                  </div>
                                  <div class="col-md-3 pull-right">
                                      <h3 class="panel-title"><? echo $car['car_brand'] ?></h3>
                                  </div>
                              </div>
                          </div>
                          <div class="panel-body">
                              <div class="row">
                                  <div class="col-md-12">
                                      <img src="<? echo MediaLink('view/images/' . $car['car_image']) ?>"
                                           class="img-responsive"/>
                                  </div>

                              </div>
                          </div>
                      </div>


                  </div>
                  <div class="col-md-3 col-md-offset-1" style="text-align: center;">
                      <div class="row">
                          <a href="/auto" class=""> назад</a>
                      </div>
                      <div class="row" style="padding-top: 20px;">
                          <a href="/buy?id=<? echo $car['car_id'] ?>" class="btn btn-info">оформить заказ</a>
                      </div>
                  </div>
              </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a href="#">Описание</a></li>
                        </ul>
                        <p><h4>Авто: </h4><? echo $car['car_name'] ?></p>
                        <p><h4>Брэнд: </h4><? echo $car['car_brand'] ?></p>
                        <p><h4>Описание: </h4><? echo $car['car_description'] ?></p>
                    </div>
                </div>

            </div>
                <? } ?>

            <? } ?>
        </div>
    </div>
</div>


<? include 'view/footer.php'; ?>
