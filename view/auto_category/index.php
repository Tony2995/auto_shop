<? include 'view/header.php'; ?>


<div class="container-fluid" style="height: 100%; padding-top: 50px;">
    <div class="row" style="height: 100%">
        <div class="col-md-2 left-menu">
            <div class="list-group">
                <? foreach ($categories as $categor) { ?>
                    <a href="<? echo "/auto/category?id=" . $categor['categor_id']; ?>" class="list-group-item"><? echo $categor['categor_name']; ?></a>
                <? } ?>
            </div>
        </div>
        <div class="col-md-10" style="padding-top: 22px;">
            <div class="container">
                <div class="row">
                    <? foreach ($cars as $car) { ?>

                        <div class="col-md-3">
                            <div class="thumbnail">
                                <h3><? echo $car['car_name'] ?></h3>
                                <img src="<? echo MediaLink('view/images/'.$car['car_image']) ?>">
                                <div class="caption">
                                    <p><? echo 'Price: '.$car['car_price'].' RUB' ?></p>
                                    <p><a href="<? echo 'auto/description?id='.$car['car_id'] ?>" class="btn btn-primary" role="button">Просмотр</a></p>
                                </div>
                            </div>
                        </div>

                    <? } ?>
                </div>
            </div>
        </div>
    </div>
</div>


<? include 'view/footer.php'; ?>
