<? include 'view/header.php'; ?>

<div class="container" style="padding-top: 100px;">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" action="" method="post">
                <fieldset>

                    <!-- Form Name -->
                    <legend class="text-center">Оформить заказ</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Ваше имя</label>
                        <div class="col-md-4">
                            <input id="name" name="order_name" type="text" placeholder="имя"
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="city">Город</label>
                        <div class="col-md-4">
                            <input id="city" name="order_city" type="text" placeholder="city"
                                   class="form-control input-md">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email">email</label>
                        <div class="col-md-4">
                            <input id="email" name="order_email" type="text" placeholder="email"
                                   class="form-control input-md">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="phone">Телефон</label>
                        <div class="col-md-4">
                            <input id="phone" name="order_phone" type="text" placeholder="телефон"
                                   class="form-control input-md">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="email"></label>
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-info" value="Оформить заказ">
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <? foreach ($cars as $car) { ?>

                    <? if ($_GET['id'] === $car['car_id']) { ?>

                    <li role="presentation" class="active"><a href="#">Описание</a></li>
                </ul>
                <div class="row">
                    <div class="col-md-6">
                        <p><h4>Авто: </h4><? echo $car['car_name'] ?></p>
                        <p><h4>Брэнд: </h4><? echo $car['car_brand'] ?></p>
                        <p><h4>Описание: </h4><? echo $car['car_description'] ?></p>
                    </div>
                    <div class="col-md-3">
                        <img src="<? echo MediaLink('view/images/' . $car['car_image']) ?>" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><h4>Итог: </h4><? echo $car['car_price'] ?> RUB</p>
                    </div>

                </div>
                    <? } ?>

                <? } ?>
            </div>
        </div>
    </div>

</div>


<? include 'view/footer.php'; ?>
