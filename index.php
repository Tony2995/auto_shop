<?php

include 'autoload.php';



if(isset($_GET['id']))
{
    $id = $_GET['id'];

}
else
{
    $id = "";
}


if ($_SERVER['REQUEST_METHOD'] === 'GET') {

  if ($root === '/')
  {

      include 'view/index/index.php';
  }
  elseif($root === '/auto')
  {
      //objects
      $categor = new Category();
      $car     = new Cars();
      //methods
      $categories = $categor->all_categories();
      $cars       = $car->all_cars();

      include 'view/auto/index.php';
  }

  elseif($root === '/auto/description?id=' . $id)
  {
      //objects
      $categor = new Category();
      $car     = new Cars();
      //methods
      $categories = $categor->all_categories();
      $cars       = $car->get_car();

      include 'view/auto_description/index.php';
  }

  elseif ($root === '/buy?id=' . $id) {
      //objects

      $car     = new Cars();
      //methods

      $cars       = $car->get_car();

      include 'view/buy_car/index.php';
  }
  elseif ($root === '/admin') {
      include 'view/admin/index.php';
  }
  elseif ($root === '/admin/all') {

      //objects
      $categor = new Category();
      $car     = new Cars();
      //methods
      $categories = $categor->all_categories();
      $cars       = $car->all_cars();

      include 'view/admin/all/index.php';
  }
  elseif ($root === '/admin/change?id=' . $id) {

      //objects

      $car = new Cars();
      //methods

      $cars = $car->all_cars();

      include 'view/admin/change_auto/index.php';
  }
  elseif ($root == '/admin/add') {

      //objects
      $categor = new Category();

      //methods
      $categories = $categor->all_categories();


      include 'view/admin/add_car/index.php';
  }
  elseif ($root == '/admin/orders') {

      $order = new Order();
      $orders = $order->all_orders();

      include 'view/admin/orders/index.php';
  }
  elseif($root === '/admin/order?id=' . $id)
  {
      //objects

      $car  = new Cars();

      $order = new Order();
      $orders = $order->get_info($id);

      include 'view/admin/order_info/index.php';
  }
  elseif($root === '/admin/categories')
  {
      $get_categories = new Category();
      $categories = $get_categories->all_categories();
      include 'view/admin/categories/index.php';
  }
  elseif($root === '/auto/category?id=' . $id)
  {
      $get_categories = new Category();
      $categories = $get_categories->all_categories();


      $car = new Cars();
      //methods

      $cars = $car->get_category_cars($id);

        include 'view/auto_category/index.php';
  }
  elseif ($root === '/admin/sold') {

      $sold = new Sale();
      $sold_orders = $sold->sold_orders();

      include 'view/admin/sold/index.php';
  }


  else
  {
      echo "404";
  }
}

elseif ($_SERVER['REQUEST_METHOD'] === "POST") {

    if ($root === '/admin/change?id=' . $id) {

      $car_name = ($_POST['car_name']);
      $car_brand = ($_POST['car_brand']);
      $car_description = ($_POST['car_description']);
      $car_price = ($_POST['car_price']);
      $car_image = ($_POST['car_image']);

      $car = new Cars();
      $cars = $car->save_car($id, $car_name, $car_brand, $car_description, $car_price, $car_image);

      header("Location: http://watch:8888/admin/all");
  }

  elseif ($root === '/admin/all') {

      $car = new Cars();
      $cars = $car->del_car($_POST['id']);

      header("Location: http://watch:8888/admin/all");
  }
  elseif ($root === '/admin/add') {



      $car_name = ($_POST['car_name']);
      $car_brand = ($_POST['car_brand']);
      $car_description = ($_POST['car_description']);
      $car_price = ($_POST['car_price']);
      $car_image = ($_POST['car_image']);
      $car_categor_id = ($_POST['car_categor_id']);

      $car = new Cars();
      $cars = $car->add_car($car_name, $car_brand, $car_description, $car_price, $car_image, $car_categor_id);

      header("Location: http://watch:8888/admin/all");

  }
    elseif ($root === '/buy?id=' . $id) {

        $order_car = $id;
        $order_name = ($_POST['order_name']);
        $order_city = ($_POST['order_city']);
        $order_email = ($_POST['order_email']);
        $order_phone = ($_POST['order_phone']);


        $order = new Order();
        $new_order = $order->new_order($order_car, $order_name, $order_city, $order_email, $order_phone);

        header("Location: http://watch:8888/auto");

    }
    elseif ($root == '/admin/orders') {

        $id = $_POST['order_id'];
        $order = new Order();
        $sale  = new Sale();

        $access_order = $order->order_access($id);
        $sale->insert_sale($id);


        header("Location: http://watch:8888/admin/orders");

    }

    elseif($root === '/admin/categories')
    {
        // add
        if (isset($_POST['categor_name'])) {
            $categor = $_POST['categor_name'];
            $get_category = new Category();
            $get_category->new_category($categor);
            header("Location: http://watch:8888/admin/categories");
            exit();

        }

        // delete
        if (isset($_POST['categor_id'])) {
            $del_id = $_POST['categor_id'];
            $categor = new Category();
            $del_categor = $categor->del_category($del_id);
            header("Location: http://watch:8888/admin/categories");
            exit();
        }

        include 'view/admin/categories/index.php';
    }

}




