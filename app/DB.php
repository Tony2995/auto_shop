<?php

class DB {
    private static $config;

    private static $connection;

    public static function configure($config) {
        if (!isset($config['dbname']) && !isset($config['username']) && !isset($config['password'])) {
            throw new Exception('Error connect DB. Check config');
        }

        self::$config = $config;
     }

    public static function getConnection() {
        if (is_null(DB::$connection)) {
            if (is_null(self::$config)) {
                throw new Exception('Error config');
            }

            $config = self::$config;

            DB::$connection = new PDO("mysql:host=localhost;dbname={$config['dbname']}", $config['username'], $config['password']);
            DB::$connection->exec('SET NAMES "utf8";');
        }

        return DB::$connection;
    }
}

